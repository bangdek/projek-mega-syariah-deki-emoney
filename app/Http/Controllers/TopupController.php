<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Profile;
use App\Models\Topup;
use App\Models\HistoryTransaction;
use Auth;
use Validator;
class TopupController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $profile = $user->profile;
        $topup = Topup::all()->where('user_id', $user->id);
        return view('dashboard.topup.index', compact('user', 'profile', 'topup'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        $topup = Topup::all()->where('user_id', $user->id);
        $profile = Profile::where('user_id', $user->id)->first();
        return view('dashboard.topup.create', compact('topup', 'profile', 'user'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();

        $validator = Validator::make($request->all(), [
            'no_rekening' => 'required|string',
            'kodepin' => ['required', 'string', 'min:6', 'max:6'],
            'jumlah_uang' => ['required', 'numeric', 'min:10.00', 'max:10000000.00']
        ]);
    
        if ($validator->fails()) {
            return view('dashboard.topup.index');
        }
        
        $profile = Profile::where('user_id', $user->id)->first();

        if($profile->validated == false){
            return view('dashboard.topup.create')->withErrors('Anda belum melakukan validasi akun lakukan validasi dahulu!');
        }
        
        $norek = $request->input('no_rekening');

        if($norek != $user->profile->no_rekening){
            return view('dashboard.topup.create')->withErrors('Nomor Rekening Tidak Valid!');
        }

        $kodepin = $request->input('kodepin');

        if($kodepin != $user->profile->kodepin){
            $gagal = 1;
            $count = $gagal++; 
            if($gagal <=3){
                return view('dashboard.topup.create')->withErrors('Kodepin anda salah! jika melebihi 3x akun anda akan di blokir');
            }else{
                $user->profile->validated =false;
                return view('dashboard.index')->withErrors('Akun anda terblokir! hubungi admin segera!');
            }
        }
        
        
        // Create data top up
        $topup = Topup::create([
            'no_rekening' => $request->no_rekening,
            'kodepin' => $request->kodepin,
            'jumlah_uang' => $request->jumlah_uang,
            'user_id' => $user->id
        ]);
        
        $topup->user_id = $user->id;
        $topup->save();
        
        
        $profile->saldo += $request->jumlah_uang;
        $profile->save();

    

        $history = new HistoryTransaction([
            'time' => now(),
            'jenis_transaksi' => 'topup',
            'jumlah' => $request->jumlah_uang,
            'rekening_tujuan' => $request->no_rekening_tujuan,
            'user_id' => $user->id
        ]);

        $history->save();

        return redirect('/topup')->back()->with('success', 'Top Up berhasil dilakukan');
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
