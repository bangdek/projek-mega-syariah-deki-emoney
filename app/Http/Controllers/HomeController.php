<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = Auth::user();
        $profile  = $user->profile;    
        return view('dashboard.index', compact('user'));
    }

    public function verify(Request $request)
    {
        $user = Auth::user();
        $profile = $user->profile;

        $request->validate([
            'nama' => ['required', 'string', 'max:255'],
            'no_telepon' => ['required', 'numeric'],
            'kodepin' => ['required', 'string', 'min:6', 'max:6'],
        ]);

        if ($profile) {
            $profile->update([
                'nama' => $request->nama,
                'no_telepon' => $request->no_telepon,
                'kodepin' => $request->kodepin,
                'validated' => true,
            ]);
        } else {
            $user->profile()->create([
                'nama' => $request->nama,
                'no_telepon' => $request->no_telepon,
                'kodepin' => $request->kodepin,
                'validated' => true,
            ]);
        }

        return redirect()->route('dashboard');
    }

    
}
