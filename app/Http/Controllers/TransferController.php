<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Auth;
use App\Models\Profile;
use App\Models\Transfer;
use App\Models\HistoryTransaction;

class TransferController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $user = Auth::user();
        $profile = $user->profile;
        $transfer = Transfer::all()->where('user_id', $user->id);
        return view('dashboard.transfer.index', compact('user', 'profile', 'transfer'));
    }

    public function create()
    {
        $user = Auth::user();
        $transfer = Transfer::all()->where('user_id', $user->id);
        $profile = Profile::where('user_id', $user->id)->first();
        return view('dashboard.transfer.create', compact('transfer', 'profile', 'user'));
    }


    public function store(Request $request)
    {
        $user = Auth::user();
        $profile = $user->profile;
        $validatedData = $request->validate([
            'no_rekening_asal' => 'required|string',
            'no_rekening_tujuan' => 'required|string',
            'kodepin' => 'required|string|min:6|max:6',
            'jumlah' => ['required', 'numeric', 'min:10.00', 'max:10000000.00']
        ]);

        $norek_asal = $profile->no_rekening;

        $userTujuan = User::whereHas('profile', function ($query) use ($validatedData) {
            $query->where('no_rekening', $validatedData['no_rekening_tujuan']);
        })->first();

        if ($norek_asal !== $validatedData['no_rekening_asal']) {
            return redirect()->back()->withErrors(['no_rekening_asal' => 'Nomor rekening harus sesuai dengan account anda!']);
        }

        if (!$userTujuan) {
            return redirect()->back()->withErrors(['no_rekening_tujuan' => 'Nomor rekening tidak ditemukan']);
        }

        $jumlah = $request->input('jumlah');
        
        if ($profile->saldo < $jumlah) {
            return redirect()->back()->withErrors(['jumlah' => 'Saldo tidak mencukupi']);
        }

        $kodepin = $request->input('kodepin');

        if ($profile->kodepin != $kodepin) {
            return redirect()->back()->withErrors(['kodepin' => 'Kode PIN salah']);
        }

        if ($userTujuan->profile->validated != 1){
            return redirect()->back()->withErrors('Nomor Rekening tujuan tidak valid!');
        }

        $transferLimit = 10000000;
        $totalTransfer = $profile->total_transfer_hari_ini; // Total transfer hari ini
        if ($jumlah > $transferLimit || $totalTransfer + $jumlah > $transferLimit) {
            return redirect()->back()->withErrors([
                'jumlah' => 'Jumlah transfer melebihi batas limit',
                'info' => "Batas limit transfer per transaksi adalah Rp " . number_format($transferLimit, 0, ',', '.') . " dan Anda sudah melakukan transfer sebesar Rp " . number_format($totalTransfer, 0, ',', '.')
            ]);
        }

        $profile->saldo -= $jumlah;
        $profile->total_transfer_hari_ini += $jumlah;
        // dd($user->profile->total_transfer_hari_ini += $jumlah);
        $profile->save();
        $user->save();

        $userTujuan->profile->saldo += $jumlah;
        $userTujuan->profile->save();

        $transfer = Transfer::create([
            "no_rekening_tujuan" => $request->no_rekening_tujuan,
            "no_rekening_asal" => $profile->no_rekening,
            "kodepin" => $request->kodepin,
            "jumlah" => $request->jumlah,
            "jumlah_uang" => $profile->saldo,
            "user_id" => $user->id
        ]);

        
        
        $history = new HistoryTransaction([
            'time' => now(),
            'jenis_transaksi' => 'transfer',
            'jumlah' => $request->jumlah,
            'rekening_tujuan' => $userTujuan->profile->no_rekening,
            'user_id' => $user->id
        ]);

        // dd($history);


        $history->save();



        return redirect('/transfer')->with('success', 'Transfer berhasil dilakukan');
    }

}
