<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Models\User;
use App\Models\Profile;
use App\Models\HistoryTransaction;
use Validator;

class AccountController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $profile  = $user->profile;
        $history = HistoryTransaction::where('user_id', $user->id)
                                        ->latest()
                                        ->take(10)
                                        ->get();
        return view('dashboard.account', compact('user', 'history'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        // Validasi input dari user
        $validatedData = $request->validate([
            'nama' => 'required|string|max:255',
            'no_telepon' => 'required|numeric',
            'kodepin' => 'required|numeric',
        ]);
    
        // Cek apakah user sudah memiliki profile
        $user = auth()->user();
        if ($user->validated === null) {
            // Buat profile baru jika user belum memiliki profile
            $profile = new Profile();
            $profile->nama = $validatedData['nama'];
            $profile->no_telepon = $validatedData['no_telepon'];
            $profile->kodepin = $validatedData['kodepin'];
            $profile->save();
    
            // Update nilai validated menjadi true
            $user->validated = true;
            $user->save();
    
            // Mengembalikan view ke halaman dashboard.index dengan membawa data user dan profile dalam sebuah variabel
            return view('dashboard.verify', [
                'user' => $user,
                'profile' => $profile,
            ]);
        } else {
            // Tampilkan pesan error jika user sudah memiliki profile
            return response()->json([
                'message' => 'Anda sudah memiliki profile',
            ], 400);
        }
    }
    

    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editProfile(Request $request)
    {
        $user = Auth::user();
    
        $validator = Validator::make($request->all(), [
            'username' => ['required', Rule::unique('users')->ignore($user->id)],
            'email' => ['required', 'email', Rule::unique('users')->ignore($user->id)],
            'password' => ['nullable', 'confirmed', 'min:6'],
        ]);
    
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
    
        $user->name = $request->input('username');
        $user->email = $request->input('email');
        
        if ($request->filled('password')) {
            $user->password = Hash::make($request->input('password'));
        }
        
        $user->save();
    
        return redirect()->back()->withSuccess('Profile berhasil di ubah!');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

     public function updateKodepin(Request $request)
     {
         // Validasi input dari user
         $validator = Validator::make($request->all(), [
            'kodepin_lama' => 'required',
            'kodepin_baru' => 'required|min:6',
            'kodepin_konfirmasi' => 'required|same:kodepin_baru',
        ]);
            
        $profile = Auth::user()->profile;

        $attemptCount = session()->get('hitung_ubah_pin', 0);

        if ($validator->fails()) {
            session()->put('hitung_ubah_pin', $attemptCount + 1);
    
            if ($attemptCount + 1 >= 3) {
                $profile->validated = false;
                $profile->save();
    
                return redirect()->back()->withErrors(['Kode PIN salah terlalu sering, akun dinonaktifkan. Hubungi CS Kami!'])->withInput();
            }
    
            return redirect()->back()->withErrors($validator)->withInput();
        }
    
        if ($request->input('kodepin_lama') != $profile->kodepin) {
            session()->put('hitung_ubah_pin', $attemptCount + 1);
    
            if ($attemptCount + 1 >= 3) {
                $profile->validated = false;
                $profile->save();
    
                return redirect()->back()->withErrors(['Kode PIN salah terlalu sering, akun dinonaktifkan. Hubungi CS Kami!'])->withInput();
            }
    
            return redirect()->back()->withErrors('Kode PIN anda salah!')->withInput();
        }
    
        $profile->kodepin = ($request->input('kodepin_baru'));
        $profile->save();
    
        session()->forget('hitung_ubah_pin');



        $profile->kodepin = $request->input('kodepin_baru');
        $profile->save();
     
        $profile->update(['validated' => true]);
     
         return redirect()->route('dashboard.index')->with('Success', 'Kode PIN anda berhasil di-ubah!');
     }
     


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
