<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\HistoryTransaction;
use App\Models\Profile;

use Auth;

class HistoryTransactionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    
    public function index(Request $request)
    {
        $user = Auth::user();
        $profile = $user->profile;
        $perPage = $request->query('per_page') ?? 10;
        $history = HistoryTransaction::where('user_id', $user->id)
                    ->orderBy('created_at')
                    ->paginate($perPage);
        return view('dashboard.history', compact('user', 'profile', 'history'));
    }
}
