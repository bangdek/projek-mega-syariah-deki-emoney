<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transfer extends Model
{
    use HasFactory;

    protected $table = 'transfer';
   
    protected $fillable = [
        'no_rekening_tujuan',
        'no_rekening_asal',
        'jumlah',
        'jumlah_uang',
        'kodepin',
        'user_id'
    ];
    
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function history()
    {
        return $this->hasMany(HistoryTransaction::class, 'rekening_tujuan', 'no_rekening')
        ->where('jenis_transaksi', 'transfer');
    }
}
