<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Transfer;
use App\Models\Topup;
use App\Models\User;

class HistoryTransaction extends Model
{
    use HasFactory;

    protected $table = 'history_transactions';

    protected $fillable = [
        'time',
        'jenis_transaksi',
        'jumlah',
        'rekening_tujuan',
        'user_id'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function topup()
    {
        return $this->belongsTo(Topup::class);
    }

    public function transfer()
    {
        return $this->belongsTo(Transfer::class);
    }

}
