<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\User;

class Profile extends Model
{
    use HasFactory;

    public $table = 'profiles';

    protected $fillable = [
        'no_telepon',
        'no_rekening',
        'saldo',
        'kodepin',
        'validated',
        'transfer_limit_hari_ini',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function topup()
    {
        return $this->hasMany(Topup::class);
    }
}
