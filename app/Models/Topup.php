<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;



class Topup extends Model
{
    use HasFactory;

    public $table = 'topups';

    protected $fillable = [
        'jumlah_uang',
        'no_rekening',
        'kodepin',
        'user_id'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function profile()
    {
        return $this->belongsTo(Profile::class);
    }

    public function history()
    {
        // return $this->hasMany(HistoryTransaction::class);
        return $this->hasMany(HistoryTransaction::class, 'rekening_tujuan', 'no_rekening')
        ->where('jenis_transaksi', 'topup');
    }
}
