<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AccountController;
use App\Http\Controllers\TopupController;
use App\Http\Controllers\TransferController;
use App\Http\Controllers\HistoryTransactionController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.register');
});

Route::middleware(['guest'])->group(function () {
});
Auth::routes();


Route::middleware(['auth'])->group(function () {

    Route::get('/dashboard', [HomeController::class, 'index'])->name('dashboard.index');
    Route::get('/dashboard/verify', [AccountController::class, 'index'])->name('dashboard.verify');
   
    Route::get('/account', [AccountController::class, 'index'])->name('account');
    Route::get('/account/edit', [AccountController::class, 'edit'])->name('account.edit');
    Route::put('/account/update', [AccountController::class, 'updateKodepin'])->name('account.update.pin');
    
    Route::get('/topup', [TopupController::class, 'index'])->name('topup.index');
    Route::get('/topup/create', [TopupController::class, 'create'])->name('dashboard.topup.create');
    Route::post('/topup', [TopupController::class, 'store'])->name('dashboard.topup.store');
    
    Route::get('/transfer', [TransferController::class, 'index'])->name('transfer.index');
    Route::get('/transfer/create', [TransferController::class, 'create'])->name('dashboard.transfer.create');
    Route::post('/transfer', [TransferController::class, 'store'])->name('dashboard.transfer.store');
    
    Route::get('/history', [HistoryTransactionController::class, 'index'])->name('history');
});



