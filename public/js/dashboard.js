/* globals Chart:false, feather:false */

(function () {
  'use strict'

  feather.replace({ 'aria-hidden': 'true' })

  // Graphs
  var ctx = document.getElementById('myChart')
  // eslint-disable-next-line no-unused-vars
  var myChart = new Chart(ctx, {
    type: 'line',
    data: {
      labels: [
        'Sunday',
        'Monday',
        'Tuesday',
        'Wednesday',
        'Thursday',
        'Friday',
        'Saturday'
      ],
      datasets: [{
        data: [
          15339,
          21345,
          18483,
          24003,
          23489,
          24092,
          12034
        ],
        lineTension: 0,
        backgroundColor: 'transparent',
        borderColor: '#007bff',
        borderWidth: 4,
        pointBackgroundColor: '#007bff'
      }]
    },
    options: {
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: false
          }
        }]
      },
      legend: {
        display: false
      }
    }
  })
})()

// // Get the checkbox
// var checkBox = document.getElementById("tosCheckbox");

// // Get the submit button
// var submitButton = document.getElementById("submitButtonVerify");

// // Disable submit button initially
// submitButton.disabled = true;

// // When the user clicks on the checkbox
// checkBox.onclick = function() {
//   // If the checkbox is checked, enable the submit button
//   if (checkBox.checked == true){
//     submitButton.disabled = false;
//   } else {
//     submitButton.disabled = true;
//   }
// }
