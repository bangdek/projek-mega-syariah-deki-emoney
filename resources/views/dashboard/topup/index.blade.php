@extends('dashboard.layouts.main')

@section('container')
<h2 class="h2 my-2">Top Up Saldo</h2>
<span class="badge rounded-pill bg-info text-dark">Pastikan anda memasukkan data yang valid dan <br> sudah melakukan verifikasi akun</span>
<div class="row my-2">
  <div class="col">
    <nav aria-label="breadcrumb" class="bg-light rounded-3 p-3 mb-4">
      <ol class="breadcrumb mb-0">
        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="#">Top up</a></li>
        <li class="breadcrumb-item active" aria-current="page">Riwayat Top Up</li>
      </ol>
    </nav>
  </div>
</div>
@if(session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
@endif
<div class="row">
  <div class="col-md-6">
    <div class="card">
      <div class="card-header">
        Saldo Anda
      </div>
      <div class="card-body">
        <h5 class="card-title">{{ 'Rp ' . number_format($user->profile->saldo, 0, ',', '.') }}</h5>
          <p class="card-text">Ini adalah saldo Anda saat ini. Klik di bawah untuk tambah saldo</p>
        <a href="{{route('dashboard.topup.create')}}" class="btn btn-success">Top Up</a>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <h5 class="h5 my-3">Riwayat Transaksi</h5>
  <div class="card my-2">
    <div class="card-body">
      <table id="topup-table" class="table table-striped table-bordered">
        <thead>
            <tr>
                <th>No</th>
                <th>Nomor Rekening</th>
                <th>Jumlah Top Up</th>
                <th>Tanggal</th>
            </tr>
        </thead>
        <tbody>
          @forelse  ( $topup as $key => $value)
          <tr>
              <td>{{$loop->index + 1}}</th> 
              <td>{{$value->no_rekening}}</td>
              <td>Rp. {{ number_format($value->jumlah_uang , 0, ',', '.') }}</td>
              <td>{{ $value->created_at }}</td>
          </tr>
          @empty
              <tr >
                <td>No data</td>
            </tr>  
          @endforelse  
        </tbody>
      </table>
    </div>
  </div>
</div>

<script>
$(function () {
    $("#topup-table").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
  });
</script>

@endsection