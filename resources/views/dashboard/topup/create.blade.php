@extends('dashboard.layouts.main')

@section('container')
<h2 class="h2 my-2">Top Up Saldo</h2>
<span class="badge rounded-pill bg-info text-dark">Pastikan anda memasukkan data yang valid dan <br> sudah melakukan verifikasi akun</span>
<div class="row my-4">
  <div class="col">
    <nav aria-label="breadcrumb" class="bg-light rounded-3 p-3 mb-4">
      <ol class="breadcrumb mb-0">
        <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
        {{-- <li class="breadcrumb-item"><a href="#">Top up</a></li> --}}
        <li class="breadcrumb-item active" aria-current="page">Top Up</li>
      </ol>
    </nav>
  </div>
</div>
@if ($errors->any())
  <div class="alert alert-danger">
    <ul>
      @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
@endif
{{-- <div class="row">
  <div class="col-sm-3">
    <p class="mb-0">No Rekening</p>
  </div>
  <div class="col-sm-9">
    <p class="text-white-100 mb-0 badge bg-info">{{$profile->no_rekening}}</p>
  </div>
</div> --}}
<div class="row">
  <div class="card my-2">
    <div class="card-body">
      <form id="topup-form" method="POST" action="/topup">
        @csrf
       <div class="mb-3">
         <label for="no_rekening" class="form-label">Nomor Rekening</label>
         <input type="number" class="form-control" id="no_rekening" name="no_rekening" required maxlength="10" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);">
        </div>
        <div class="mb-3">
          <label for="kodepin" class="form-label">Kode PIN</label>
          <input type="password" name="kodepin" class="form-control" id="kodepin" required maxlength="6" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);">
        </div>
        <div class="mb-3">
          <label for="jumlah_uang" class="form-label">Jumlah top up</label>
          <div class="input-group">
            <span class="input-group-text">Rp</span>
            <input type="number" name="jumlah_uang" class="form-control" id="jumlah_uang" required maxlength="12" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);">
          </div>
        </div>
        {{-- <input type="hidden" name="user_id" value="{{ $user->profile->user_id }}"> --}}
        <button type="submit" class="btn btn-primary">{{ __('Top Up') }}</button>
      </form>
    </div>
  </div>
</div>

<script>
function showPassword() {
    let passwordField = document.getElementById("kodepin");
    passwordField.type = "text";
  }

  function hidePassword() {
    let passwordField = document.getElementById("kodepin");
    passwordField.type = "password";
  }

  document.getElementById("topup-form").addEventListener("submit", function(event) {
    event.preventDefault(); // Mencegah form dari langsung dikirim

    //konfirmasi
    if (confirm("Apakah anda yakin ingin melakukan top up?")) {
      // Jika user mengklik "OK" pada konfirmasi, submit form
      this.submit();
    }
  });
</script>

@endsection