@extends('dashboard.layouts.main')

@section('container')
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
  <h1 class="h2">My Account</h1>
  <div class="d-flex align-items-center">
    <i class="bi bi-wallet-fill me-2"></i>
    <i class="bi bi-bell-fill ms-4"></i>
  </div>
</div>
<section style="background-color: #eee;">
  <div class="container py-5">
    <div class="row">
      <div class="col">
        <nav aria-label="breadcrumb" class="bg-light rounded-3 p-3 mb-4">
          <ol class="breadcrumb mb-0">
            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="#">Account</a></li>
            <li class="breadcrumb-item active" aria-current="page">My Account</li>
          </ol>
        </nav>
      </div>
    </div>
    @if(session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
    @endif
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
    @endif
    <div class="row">
      <div class="col-lg-4">
        <div class="card mb-4">
          <div class="card-body text-center">
            <img src="https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-chat/ava3.webp" alt="avatar"
              class="rounded-circle img-fluid" style="width: 150px;">
            <h5 class="my-3">{{$user->username}}</h5>
            <p class="text-muted mb-1">Full Stack Developer</p>
           <h6 class="text-muted mb-4"><i class="fas fa-wallet"></i> {{$user->profile->saldo}}</h6>
            @if($user->profile->validated)
            <div class="alert alert-success " role="alert">
              <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-check-circle-fill mx-2" viewBox="0 0 16 16">
                <path d="M8 0.5a7.5 7.5 0 1 1 0 15 7.5 7.5 0 0 1 0-15zm3.354 5.854a.5.5 0 0 0-.708-.708L7.5 9.793 6.354 8.646a.5.5 0 0 0-.708.708l1.5 1.5a.5.5 0 0 0 .708 0l3-3z"/>
              </svg>  
              <div>
                Akun sudah terverifikasi
              </div>
            </div>
            @else
                <div class="alert alert-warning" role="alert">
                    Akun Anda belum terverifikasi. Silakan <a href="{{ route('account.edit') }}" class="alert-link">klik disini</a> untuk melakukan verifikasi akun agar dapat melakukan transaksi!
                </div>
            @endif
            <div class="d-flex justify-content-center mb-2">
              <button type="button" class="btn btn-primary">Follow</button>
              <button type="button" class="btn btn-outline-primary ms-1">Message</button>
            </div>
          </div>
        </div>
        <h5 class="h5">Transaksi Terkini</h5>
        <div class="card mb-4 mb-lg-0">
          <div class="card-body p-0">
            <ul class="list-group list-group-flush rounded-3">
              {{-- <li class="list-group-item d-flex justify-content-between align-items-center p-3">
                <i class="fas fa-globe fa-lg text-warning"></i>
                <p class="mb-0">https://mdbootstrap.com</p>
              </li> --}}
              @forelse  ( $history as $key => $value)
              <li class="list-group-item d-flex justify-content-between align-items-center p-3">
                <i class="fas fa-envelope fa-lg" style="color: #007bff;"></i>
                <h6 class="h6">Transaksi {{$value->jenis_transaksi}} berhasil</h6>
                <p class="text-muted small float-end">{{$value->created_at->format('d M Y')}}</p>
              </li>
              
              @empty
                  <tr >
                    <td>No data</td>
                </tr>  
              @endforelse  
            </ul>
          </div>
        </div>
      </div>
      <div class="col-lg-8">
        <div class="card mb-4">
          <div class="card-body">
            <div class="row">
              <div class="col-sm-3">
                <p class="mb-0">Nama Lengkap</p>
              </div>
              <div class="col-sm-9">
                <p class="text-muted mb-0">{{$user->profile->nama}}</p>
              </div>
            </div>
            <hr>
            <div class="row">
              <div class="col-sm-3">
                <p class="mb-0">Email</p>
              </div>
              <div class="col-sm-9">
                <p class="text-muted mb-0">{{$user->email}}</p>
              </div>
            </div>
            <hr>
            <div class="row">
              <div class="col-sm-3">
                <p class="mb-0">No Telepon</p>
              </div>
              <div class="col-sm-9">
                <p class="text-muted mb-0">{{$user->profile->no_telepon}}</p>
              </div>
            </div>
            <hr>
            <div class="row">
              <div class="col-sm-3">
                <p class="mb-0">No Rekening</p>
              </div>
              <div class="col-sm-9">
                <p class="text-white-100 mb-0 badge bg-info">{{$user->profile->no_rekening}}</p>
              </div>
            </div>
            <hr>
            <div class="row">
              <div class="col-sm-3">
                <p class="mb-0">Address</p>
              </div>
              <div class="col-sm-9">
                <p class="text-muted mb-0">Bay Area, San Francisco, CA</p>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <div class="card mb-4 mb-md-0">
              <form action="/profile/update" method="POST">
                @csrf
                <div class="card-body">
                  <p class="mb-4"><span class="text-primary font-italic me-1">Edit Account</span> Edit Profile</p>
                
                  <div class="form-group mb-3">
                    <label for="username" class="form-label">Username</label>
                    <input type="text" class="form-control" name="username" value="{{$user->name}}" required>
                  </div>
              
                  <div class="form-group mb-3">
                    <label for="email" class="form-label">Email</label>
                    <input type="email" class="form-control" name="email" value="{{$user->email}}" required>
                  </div>
              
                  <div class="form-group mb-3">
                    <label for="password" class="form-label">New Password</label>
                    <input type="password" class="form-control" name="password">
                  </div>
              
                  <div class="form-group mb-3">
                    <label for="password_confirmation" class="form-label">Confirm Password</label>
                    <input type="password" class="form-control" name="password_confirmation">
                  </div>
              
                  <button type="submit" class="btn btn-primary">Update Profile</button>
                </div>
              </form>
            </div>
          </div>
          <div class="col-md-6">
            <div class="card mb-4 mb-md-0">
            <form id="formKodein" action="{{route('account.update.pin')}}" method="POST">
              @method('PUT')
              @csrf
            
              <div class="card-body">
                <p class="mb-4"><span class="text-primary font-italic me-1">Change PIN</span> Project Status</p>
            
                <div class="form-group mb-3">
                  <label for="current_kodepin" class="form-label">Current PIN</label>
                  <input type="password" class="form-control" name="kodepin_lama" required>
                </div>
            
                <div class="form-group mb-3">
                  <label for="new_kodepin" class="form-label">New PIN</label>
                  <input type="password" class="form-control" name="kodepin_baru" required>
                </div>
            
                <div class="form-group mb-3">
                  <label for="confirm_kodepin" class="form-label">Confirm New PIN</label>
                  <input type="password" class="form-control" name="kodepin_konfirmasi" id required>
                </div>
            
                <button type="submit" class="btn btn-primary">Update PIN</button>
              </div>
            </form>
            
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<script>
// Ambil form
const form = document.getElementsById('formKodepin');

// Tambahkan event listener pada submit button
form.addEventListener('submit', (event) => {
  // Berhenti melakukan submit secara default
  event.preventDefault();

  // Tampilkan alert konfirmasi
  const isConfirmed = confirm('Apakah Anda yakin ingin mengubah kode PIN?');

  // Jika user mengkonfirmasi perubahan kode PIN
  if (isConfirmed) {
    // Kirim data ke server dengan AJAX atau fetch API
    // ...

    // Tampilkan alert berhasil jika kode PIN berhasil diubah
    const alert = document.createElement('div');
    alert.classList.add('alert', 'alert-success', 'my-3');
    alert.textContent = 'Kode PIN berhasil diubah!';
    form.parentNode.insertBefore(alert, form.nextSibling);
  }
});

</script>
@endsection