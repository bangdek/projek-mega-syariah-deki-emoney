@extends('dashboard.layouts.main')

@section('container')
<h2 class="h2 my-2">Transfer Saldo</h2>
<span class="badge rounded-pill bg-info text-dark">Pastikan anda memasukkan data yang valid dan <br> sudah melakukan verifikasi akun</span>
<div class="row my-4">
  <div class="col">
    <nav aria-label="breadcrumb" class="bg-light rounded-3 p-3 mb-4">
      <ol class="breadcrumb mb-0">
        <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
        {{-- <li class="breadcrumb-item"><a href="#">Top up</a></li> --}}
        <li class="breadcrumb-item active" aria-current="page">Transfer</li>
      </ol>
    </nav>
  </div>
</div>
@if ($errors->any())
  <div class="alert alert-danger">
    <ul>
      @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
@endif
<div class="row">
  <div class="card my-2">
    <div class="card-body">
      <form id="transfer-form" method="POST" action="/transfer">
        @csrf
        <div class="mb-3">
          <label for="no_rekening_asal" class="form-label">Nomor Rekening Asal</label>
          <input type="number" class="form-control" id="no_rekening_asal" name="no_rekening_asal" required maxlength="10" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);">
        </div>
        <div class="mb-3">
          <label for="no_rekening_tujuan" class="form-label">Nomor Rekening Tujuan</label>
          <input type="number" class="form-control" id="no_rekening_tujuan" name="no_rekening_tujuan" required maxlength="10" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);">
        </div>
        <div class="mb-3">
          <label for="jumlah" class="form-label">Jumlah Transfer</label>
          <div class="input-group">
            <span class="input-group-text">Rp</span>
            <input type="number" name="jumlah" class="form-control" id="jumlah" required maxlength="12" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);">
          </div>
        </div>
        <div class="mb-3">
          <label for="kodepin" class="form-label">Kode PIN</label>
          <input type="password" name="kodepin" class="form-control" id="kodepin" required maxlength="6" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);">
        </div>
        <button type="submit" class="btn btn-primary">{{ __('Transfer') }}</button>
      </form>
    </div>
  </div>
</div>

<script>
function showPassword() {
    let passwordField = document.getElementById("kodepin");
    passwordField.type = "text";
  }

  function hidePassword() {
    let passwordField = document.getElementById("kodepin");
    passwordField.type = "password";
  }

  document.getElementById("topup-form").addEventListener("submit", function(event) {
    event.preventDefault(); // Mencegah form dari langsung dikirim

    //konfirmasi
    if (confirm("Apakah anda yakin ingin melakukan transfer?")) {
      // Jika user mengklik "OK" pada konfirmasi, submit form
      this.submit();
    }
  });
</script>

@endsection