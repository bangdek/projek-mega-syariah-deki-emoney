@extends('dashboard.layouts.main')

@section('container')
<h2 class="h2 my-2">Riwayat Transaksi</h2>
<span class="badge rounded-pill bg-info text-dark">Pastikan anda memasukkan data yang valid dan <br> sudah melakukan verifikasi akun</span>
<div class="row my-2">
  <div class="col">
    <nav aria-label="breadcrumb" class="bg-light rounded-3 p-3 mb-4">
      <ol class="breadcrumb mb-0">
        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="#">Transaction History</a></li>
      </ol>
    </nav>
  </div>
</div>
<div class="row">
  <div class="col-md-6">
    <div class="card">
      <div class="card-header">
        Saldo Anda
      </div>
      <div class="card-body">
        <h5 class="card-title">{{ 'Rp ' . number_format($user->profile->saldo, 0, ',', '.') }}</h5>
          <p class="card-text">Ini adalah saldo Anda saat ini. Klik di bawah untuk transaksi</p>
        <a href="{{route('dashboard.topup.create')}}" class="btn btn-success">Top Up</a>
        <a href="{{route('dashboard.transfer.create')}}" class="btn btn-warning">Transfer</a>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <h5 class="h5 my-3">Riwayat Semua Transaksi</h5>
  <div class="card my-2">
    <div class="card-body">
      <table id="history-table" class="table table-striped table-bordered">
        <p>Select Showed Data</p>
        <select onchange="this.options[this.selectedIndex].value && (window.location.href = '{{ url()->current() }}?per_page=' + this.options[this.selectedIndex].value);">
          <option value="5" {{ request('per_page') == '5' ? 'selected' : '' }}>5</option>
          <option value="10" {{ request('per_page') == '10' || !request()->has('per_page') ? 'selected' : '' }}>10</option>
          <option value="20" {{ request('per_page') == '20' ? 'selected' : '' }}>20</option>
        </select>      
        <thead>
            <tr>
                <th>No</th>
                <th>Jenis Transaksi</th>
                <th>Nomor Rekening Tujuan</th>
                <th>Jumlah Transfer</th>
                <th>Time</th>
            </tr>
        </thead>
        <tbody>
          @forelse  ( $history as $key => $value)
          <tr>
              <td>{{$loop->index + 1}}</th> 
              <td>{{$value->jenis_transaksi}}</td>
              <td>{{$value->rekening_tujuan}}</td>
              <td>Rp. {{ number_format($value->jumlah , 0, ',', '.') }}</td>
              <td>{{ $value->time }}</td>
          </tr>
          @empty
              <tr >
                <td>No data</td>
            </tr>  
          @endforelse  
        </tbody>
      </table>
      <div class="card-footer">
        {{$history->links()}}
      </div>
    </div>
  </div>
</div>

<script>
  $(document).ready(function () {
  $('#dtBasicExample').DataTable();
  $('.dataTables_length').addClass('bs-select');
});
// // Ambil elemen dropdown
// var perPageSelect = document.getElementById('perPageSelect');

// // Ketika pilihan diubah
// perPageSelect.addEventListener('change', function() {
//     // Ambil nilai opsi yang dipilih
//     var perPage = this.value;

//     // Redirect ke halaman yang sama dengan query string 'per_page' yang diatur sesuai nilai opsi yang dipilih
//     window.location.href = window.location.pathname + '?per_page=' + perPage;
// });
</script>


@endsection