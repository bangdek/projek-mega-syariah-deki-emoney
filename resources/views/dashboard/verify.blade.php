@extends('dashboard.layouts.main')

@section('container')
  <div class="container">
    <h2 class="h2 my-2">Verifikasi Akun</h2>
  <div class="row justify-content-center">
    <h3 class="text-primary my-3">Selamat datang {{$user->username}}!</h3>
    @if($user->profile->validated)
        <span class="badge rounded-pill bg-success text-light my-3">Akun terverifikasi</span>
    @else
        <div class="alert alert-warning" role="alert">
            Akun Anda belum terverifikasi. Mohon lakukan verifikasi dengan melengkapi data dibawah ini!
        </div>
    @endif      
    
    <!-- Button trigger modal -->
<button type="button" class="btn btn-md btn-primary" data-bs-toggle="modal" data-bs-target="#termOfServiceModal">
  Lihat Term of Service
</button>

    <!-- Modal -->
    <div class="modal fade" id="termOfServiceModal" tabindex="-1" aria-labelledby="termOfServiceModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-dialog-scrollable modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="termOfServiceModalLabel">Term of Service</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
            <!-- isi term of service di sini -->
            <p>Terkait dengan pembukaan rekening, kami ingin memberikan informasi mengenai persyaratan dan ketentuan yang perlu dipahami sebelum Anda melakukan pembukaan rekening. Dalam hal ini, dengan membuka rekening di institusi kami, Anda setuju untuk mematuhi ketentuan-ketentuan yang berlaku, termasuk namun tidak terbatas pada persyaratan mengenai identitas, transaksi keuangan, dan penggunaan layanan perbankan online. Kami juga berhak untuk membatasi akses Anda ke layanan kami jika ditemukan adanya pelanggaran terhadap ketentuan tersebut. Oleh karena itu, sebelum memutuskan untuk membuka rekening, Anda diharapkan untuk mempelajari dengan seksama seluruh persyaratan dan ketentuan yang tertera dalam syarat dan ketentuan pembukaan rekening yang kami sediakan. Dengan membuka rekening, Anda dianggap telah membaca, memahami, dan setuju dengan seluruh ketentuan yang berlaku.</p>      
            <!-- end isi term of service -->
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>

    <!-- Checklist untuk menyetujui term of service -->
    <div class="form-check mt-3">
      <input class="form-check-input" type="checkbox" value="" id="agreeToTermsCheckbox" required>
      <label class="form-check-label" for="agreeToTermsCheckbox">
        Saya telah membaca dan menyetujui Term of Service
      </label>
      <div class="invalid-feedback">
        Anda harus menyetujui Term of Service untuk melanjutkan
      </div>
    </div>

    <div class="col-md-12">
        <div class="card">
            <div class="card-header">{{ __('Verification') }}</div>

            <div class="card-body">
                <form method="POST" action="{{ route('account.update') }}">
                    @csrf
                    @method('PUT')
                    <div class="form-group row">
                        <label for="nama" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>
                        <div class="col-md-8 my-2">
                            <input id="nama" type="text" class="form-control @error('nama') is-invalid @enderror" name="nama" value="{{ old('nama') }}" required autocomplete="nama" autofocus>

                            @error('nama')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                      <label for="no_telepon" class="col-md-4 col-form-label text-md-right">{{ __('Phone Number') }}</label>
                  
                      <div class="col-md-8 my-2">
                          <input id="no_telepon" type="number" class="form-control @error('no_telepon') is-invalid @enderror" name="no_telepon" value="{{ old('no_telepon') }}" required autocomplete="no_telepon" maxlength="12" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);">
                  
                          @error('no_telepon')
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                          @enderror
                        </div>
                    </div>                  
                    <div class="form-group row">
                      <label for="kodepin" class="col-md-4 col-form-label text-md-right">{{ __('New PIN') }}</label>
                  
                      <div class="col-md-8 my-2">
                          <input  maxlength="6" mminlength="6" id="kodepin" type="text" class="form-control @error('kodepin') is-invalid @enderror" name="kodepin" required autocomplete="kodepin" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);">
                          @error('kodepin')
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                          @enderror
                          </div>
                      </div>
                    
                    <div class="form-group row">
                        <label for="kodepin_confirmation" class="col-md-4 col-form-label text-md-right">{{ __('Confirm New PIN') }}</label>
                    
                        <div class="col-md-8 my-2">
                            <input maxlength="6" mminlength="6"  id="kodepin_confirmation" type="text" class="form-control" name="kodepin_confirmation" required  oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);">
                        </div>
                    </div>
                    <div class="form-group row mb-0">
                        <div class="col-md-8 my-2 offset-md-4">
                            <button type="submit" class="btn btn-primary">
                                {{ __('Verify') }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
  </div>
  </div>
  {{-- <script>
    const agreeButton = document.querySelector('#agreeToTermsCheckbox');
    agreeButton.addEventListener('click', () => {
      console.log('User agreed to the Terms of Service');
    });
  </script> --}}
@endsection