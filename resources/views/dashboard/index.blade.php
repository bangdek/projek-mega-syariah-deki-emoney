@extends('dashboard.layouts.main')

@section('container')
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
  <h1 class="h2">Dashboard</h1>
  <div class="d-flex align-items-center">
    <i class="bi bi-wallet-fill me-2"></i>
    <div class="position-relative">
      <span class="badge rounded-pill bg-primary text-light">{{ 'Rp ' . number_format($user->profile->saldo, 0, ',', '.') }}</span>
      <button type="button" class="btn btn-link btn-sm position-absolute top-100 start-100 translate-middle" data-bs-toggle="modal" data-bs-target="#topUpModal">
        <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" fill="currentColor" class="bi bi-wallet-fill ms-3" viewBox="0 0 16 16">
          <path d="M2 5a2 2 0 0 1 2-2h10a2 2 0 0 1 2 2v9a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V5zm6 1a1 1 0 0 0-1 1v1a1 1 0 1 0 2 0V7a1 1 0 0 0-1-1zM5 7a1 1 0 0 0-1 1v1a1 1 0 1 0 2 0V8a1 1 0 0 0-1-1zm6 5a2 2 0 0 1-2 2 2 2 0 0 1-2-2h4zm-3-3a1 1 0 1 1 0-2 1 1 0 0 1 0 2z"/>
        </svg>
      </button>
    </div>
    <i class="bi bi-bell-fill ms-4"></i>
  </div>
</div>
@if ($errors->any())
<div class="alert alert-danger">
  <ul>
    @foreach ($errors->all() as $error)
      <li>{{ $error }}</li>
    @endforeach
  </ul>
</div>
@endif
<h3 class="text-primary">Selamat datang {{$user->username}}!</h3>
@if($user->profile->validated)
<div class="alert alert-success d-flex align-items-center" role="alert">
  <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-check-circle-fill mx-2" viewBox="0 0 16 16">
    <path d="M8 0.5a7.5 7.5 0 1 1 0 15 7.5 7.5 0 0 1 0-15zm3.354 5.854a.5.5 0 0 0-.708-.708L7.5 9.793 6.354 8.646a.5.5 0 0 0-.708.708l1.5 1.5a.5.5 0 0 0 .708 0l3-3z"/>
  </svg>  
  <div>
    Akun sudah terverifikasi
  </div>
</div>
@else
    <div class="alert alert-warning" role="alert">
        Akun Anda belum terverifikasi. Silakan <a href="{{ route('account.edit') }}" class="alert-link">klik disini</a> untuk melakukan verifikasi akun agar dapat melakukan transaksi!
    </div>
@endif      
<div class="row">
  <div class="col-md-6">
    <div class="card">
      <div class="card-header">
        Saldo Anda
      </div>
      <div class="card-body">
        <h5 class="card-title">{{ 'Rp ' . number_format($user->profile->saldo, 0, ',', '.') }}</h5>
          <p class="card-text">Ini adalah saldo Anda saat ini. Klik di bawah untuk tambah saldo</p>
        <a href="{{route('dashboard.topup.create')}}" class="btn btn-success">Top Up</a>
      </div>
    </div>
  </div>
  <div class="col-md-6">
    <div class="card">
      <div class="card-header">
        Featured
      </div>
      <div class="card-body">
        <h5 class="card-title">Special title treatment</h5>
        <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
        <a href="#" class="btn btn-primary">Go somewhere</a>
      </div>
    </div>
  </div>
</div>
@endsection