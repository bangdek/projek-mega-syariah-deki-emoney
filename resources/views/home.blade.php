@extends('layouts.app')
@section('content')
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Deki Riana">
    <title>BMS | Dashboard</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/5.1/examples/dashboard/">

    

    <!-- Bootstrap core CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    
    <!-- Custom styles for this template -->
    <link href="/css/dashboard.css" rel="stylesheet">
  </head>
  <body>
    
    <header class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0 shadow">
        <a class="navbar-brand col-md-3 col-lg-2 me-0 px-3" href="#">E-Money BMS</a>
        <button class="navbar-toggler position-absolute d-md-none collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#sidebarMenu" aria-controls="sidebarMenu" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <input class="form-control form-control-dark w-100" type="text" placeholder="Search" aria-label="Search">
        <div class="navbar-nav">
          <div class="nav-item text-nowrap">
            <a class="nav-link px-3" href="{{ route('logout') }}"
               onclick="event.preventDefault();
                       document.getElementById('logout-form').submit();">
                {{ __('Sign out') }}
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                @csrf
            </form>
          </div>
        </div>
    </header>


<div class="container-fluid">
  <div class="row">
    <nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">
      <div class="position-sticky pt-3">
        <ul class="nav flex-column">
          <li class="nav-item">
            <a class="nav-link active" aria-current="page" href="#">
              <span data-feather="home"></span>
              Dashboard
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">
              <span data-feather="file"></span>
              Orders
            </a>
          </li>
        </ul>

        <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
          <span>Saved reports</span>
          <a class="link-secondary" href="#" aria-label="Add a new report">
            <span data-feather="plus-circle"></span>
          </a>
        </h6>
        <ul class="nav flex-column mb-2">
          <li class="nav-item">
            <a class="nav-link" href="#">
              <span data-feather="file-text"></span>
              Current month
            </a>
          </li>
        </ul>
      </div>
    </nav>
  </div>

    <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
      <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Dashboard</h1>
        {{-- <div class="d-flex align-items-center">
          <span class="me-2">Saldo Anda:</span>
          <span class="badge bg-primary text-light">{{ 'Rp ' . number_format($user->profile->saldo, 0, ',', '.') }}</span>
          <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" fill="currentColor" class="bi bi-wallet-fill ms-3" viewBox="0 0 16 16">
            <path d="M2 5a2 2 0 0 1 2-2h10a2 2 0 0 1 2 2v9a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V5zm6 1a1 1 0 0 0-1 1v1a1 1 0 1 0 2 0V7a1 1 0 0 0-1-1zM5 7a1 1 0 0 0-1 1v1a1 1 0 1 0 2 0V8a1 1 0 0 0-1-1zm6 5a2 2 0 0 1-2 2 2 2 0 0 1-2-2h4zm-3-3a1 1 0 1 1 0-2 1 1 0 0 1 0 2z"/>
          </svg>
        </div> --}}
        <!-- Icon wallet dan toggle top up -->
<div class="d-flex align-items-center">
  <i class="bi bi-wallet-fill me-2"></i>
  <div class="position-relative">
    <span class="badge rounded-pill bg-primary text-light">{{ 'Rp ' . number_format($user->profile->saldo, 0, ',', '.') }}</span>
    <button type="button" class="btn btn-link btn-sm position-absolute top-100 start-100 translate-middle" data-bs-toggle="modal" data-bs-target="#topUpModal">
      <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" fill="currentColor" class="bi bi-wallet-fill ms-3" viewBox="0 0 16 16">
        <path d="M2 5a2 2 0 0 1 2-2h10a2 2 0 0 1 2 2v9a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V5zm6 1a1 1 0 0 0-1 1v1a1 1 0 1 0 2 0V7a1 1 0 0 0-1-1zM5 7a1 1 0 0 0-1 1v1a1 1 0 1 0 2 0V8a1 1 0 0 0-1-1zm6 5a2 2 0 0 1-2 2 2 2 0 0 1-2-2h4zm-3-3a1 1 0 1 1 0-2 1 1 0 0 1 0 2z"/>
      </svg>
    </button>
  </div>
</div>

<!-- Modal top up -->
<div class="modal fade" id="topUpModal" tabindex="-1" aria-labelledby="topUpModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="topUpModalLabel">Top Up Saldo</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <!-- Form top up -->
        <form>
          <div class="mb-3">
            <label for="topUpAmount" class="form-label">Jumlah top up</label>
            <div class="input-group">
              <span class="input-group-text">Rp</span>
              <input type="number" class="form-control" id="topUpAmount" required>
            </div>
          </div>
          <div class="mb-3">
            <label for="topUpMethod" class="form-label">Metode pembayaran</label>
            <select class="form-select" id="topUpMethod" required>
              <option value="" selected disabled>Pilih metode pembayaran</option>
              <option value="1">Transfer Bank</option>
              <option value="2">OVO</option>
              <option value="3">DANA</option>
            </select>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
        <button type="button" class="btn btn-primary">Bayar</button>
      </div>
    </div>
  </div>
</div>

    </div>      
      <h3 class="text-primary">Selamat datang {{$user->username}}!</h3>
      @if($user->profile->validated)
          <span class="badge rounded-pill bg-success text-light my-3">Akun terverifikasi</span>
      @else
          <div class="alert alert-warning" role="alert">
              Akun Anda belum terverifikasi. Silakan <a href="{{ route('dashboard.verify') }}" class="alert-link">klik disini</a> untuk melakukan verifikasi akun agar dapat melakukan transaksi!
          </div>
      @endif      
      <div class="row">
        <div class="col-md-6">
          <div class="card">
            <div class="card-header">
              Saldo Anda
            </div>
            <div class="card-body">
              <h5 class="card-title">{{ 'Rp ' . number_format($user->profile->saldo, 0, ',', '.') }}</h5>
                <p class="card-text">Ini adalah saldo Anda saat ini. Klik di bawah untuk tambah saldo</p>
              <a href="#" class="btn btn-primary">Top Up</a>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="card">
            <div class="card-header">
              Featured
            </div>
            <div class="card-body">
              <h5 class="card-title">Special title treatment</h5>
              <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
              <a href="#" class="btn btn-primary">Go somewhere</a>
            </div>
          </div>
        </div>
      </div>
    </main>
  </div>
</div>


<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

<script src="https://cdn.jsdelivr.net/npm/feather-icons@4.28.0/dist/feather.min.js" integrity="sha384-uO3SXW5IuS1ZpFPKugNNWqTZRRglnUJK6UAZ/gxOX80nxEkN9NcGZTftn6RzhGWE" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.4/dist/Chart.min.js" integrity="sha384-zNy6FEbO50N+Cg5wap8IKA4M/ZnLJgzc6w2NqACZaK0u0FXfOWRRJOnQtpZun8ha" crossorigin="anonymous">
</script><script src="/js/dashboard.js"></script>
<script>
  const toggleBtn = document.querySelector('.toggle-btn');
  const modal = document.querySelector('.modal');

  toggleBtn.addEventListener('click', () => {
    modal.classList.toggle('show');
  });
</script>

  </body>
</html>
@endsection
